<?php
require_once 'config.php';

error_reporting(E_ALL); 
ini_set("display_errors", 1	); 

// Quelle action ? Exécution de l'action demandé
$action = empty($_REQUEST['action']) ? 'home' : $_REQUEST['action'];
//include_once PATH_MODELS.'/'.$action.'.php';
include_once PATH_CONTROLERS.'/'.$action.'.php';

// Rendu du templating Action
ob_start();
include_once PATH_VIEWS.'/'.$action.'.php';
$content = ob_get_contents();
ob_end_clean();

// Rendu du templating Global
if (!($action == "Formulaires/export")){
include_once PATH_CONTROLERS.'/layout.php';
include_once PATH_VIEWS.'/layout.php';
}
?>
	