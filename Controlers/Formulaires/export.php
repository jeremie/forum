<?php 
require PATH_MODELS."/post.php";
require PATH_MODELS."/user.php";
require PATH_MODELS."/categorie.php";
$post = new post();
$user = new user();
$cate = new categorie();
$page_title="export en xml";
$var = $post->recupAllMessage();
header("Content-type: text/xml");
header('Content-Disposition: attachment; filename="downloaded.xml"');


$xml_output = "<?xml version=\"1.0\"?>\n";
$xml_output .= "<Messages>\n";

for($x = 0 ; $x < count($var) ; $x++){

	$pseudo =$user->afficheNomUser($var[$x]->ID_AUTEUR_POST);
	$categorie = $cate->recupCategorie($var[$x]->ID_CATEGORIE);

    $xml_output .= "\t<Message>\n";
    	$xml_output .= "\t\t<id>";
			$xml_output .=$var[$x]->ID_POST;
		$xml_output .= "</id>\n";

		$xml_output .= "\t\t<nom>";
			$xml_output .= $pseudo->PSEUDO_USER;
		$xml_output .= "</nom>";

		$xml_output .= "\t\t<date>";
			$xml_output .=$var[$x]->DATE_POST;
		$xml_output .= "\</date>\n";

		$xml_output .= "\t\t<contenue>";
			$xml_output .=$var[$x]->CONTENUE_POST;
		$xml_output .= "\</contenue>\n";

		$xml_output .= "\t\t<categorie>	";
			$xml_output .=$categorie->NOM_CATEGORIE;
		$xml_output .= "</categorie>\n";

    $xml_output .= "\t</Message>\n";
}

$xml_output .= "</Messages>";

echo $xml_output;
?> 