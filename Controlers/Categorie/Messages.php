<?php 
require PATH_MODELS."/post.php";
require PATH_MODELS."/user.php";
require PATH_MODELS."/categorie.php";
$post = new post();
$user = new user();
$cate = new categorie();
session_start();
$link ="";
$suppr="";
$edit="";
if ( !empty($_POST["idCate"]) ) 
{
	$_SESSION['idCategorie'] = $_POST["idCate"];
}

if (isset($_SESSION['id'])){
	$Iddent = $_SESSION['id'];
}
else
	$Iddent="";

$nomCate=$cate->recupCategorie($_SESSION['idCategorie'])->NOM_CATEGORIE;
$Messages =$post->afficheMessages($_SESSION['idCategorie']);
if ( !empty($_SESSION['id']))
	$link = "<a href=\"?action=Categorie/newMessage\">Nouveau message</a>";

for ($i=0; $i < count($Messages) ; $i++) { 
	$idPost[$i]= $Messages[$i]->ID_POST;
	$datePost[$i] = $Messages[$i]->DATE_POST;
	$contenuePost[$i] = $Messages[$i]->CONTENUE_POST;
	$id[$i] = $Messages[$i]->ID_AUTEUR_POST;
	$nom[$i] = $user->afficheUser($Messages[$i]->ID_AUTEUR_POST);
	$nomUser[$i] = $nom[$i]->PSEUDO_USER;



	if (!empty($_SESSION['admin']) || $Iddent === $id[$i] ){

		$edit[$i] = "
		<div class=\"aDroite btn\">
			<form class=\"memeLigne\" method=\"post\" action=\"?action=Formulaires/editerMessage\" class=\"messages\">
			<input type=\"text\" name=\"idPost\" value =".$idPost[$i]."  style =\"display: none\"/> 
			<input class=\"btn bouton\" type=\"submit\" value=\"Editer\"/>
			</form>";

		$suppr[$i] = "
			<form class=\"memeLigne\" method=\"post\" action=\"?action=Formulaires/supprimerMessage\" class=\"messages\">
			<input type=\"text\" name=\"idPost\" value =".$idPost[$i]."  style =\"display: none\"/> 
			<button class=\"btn bouton rouge\" type=\"submit\" name=\"submit\" value=\"valider\">
				<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"> Supprimer</span>
			</button>
			</form>
		</div>";
	}
	else {
		$suppr[$i]=" ";
		$edit[$i]=" ";
	}
}

$page_title="Liste des messages";
?>