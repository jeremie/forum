<?php 
require PATH_MODELS."/categorie.php";

$page_title="Creation d'une nouvelle categorie";
$categorie = new Categorie();
$message="";
	if (empty($_POST['nomCate']) || empty($_POST['description']) ) 
	{
		$message= "Remplir tout les champs";
	}
	else
	{
		if ($categorie->ajoutCategorie($_POST['nomCate'], $_POST['description'])){
			header('location: ?action=home');	
		}
		else
			$message=$categorie->message_error();
	}
?>