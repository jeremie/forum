<?php 
require PATH_MODELS."/post.php";
session_start();
$page_title= "Nouveau message";
$post = new post();

$message="";
if (!empty($_SESSION['idCategorie']) && !empty($_SESSION['id'])){
	if(empty($_POST['contenue_post']))
		$message="Le message ne peut être vide";
	else
		if($post->saveMessage($_SESSION['idCategorie'],$_SESSION['id'],$_POST['contenue_post']))
			header('location: ?action=Categorie/Messages');
}

 ?>