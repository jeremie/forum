<?php
require PATH_MODELS."/categorie.php";
require PATH_MODELS."/user.php";
session_start();
$link ="";
$suppr="";
$edit="";
$categories = new categorie();
$users = new user();
$ListUsers="<a href=\"?action=Formulaires/listUser\">Liste des utilisateurs</a>";
$page_title = "Bienvenue"; 	

if (!empty($_SESSION['id']))
	$link = "<a href=\"?action=Categorie/newCategorie\">Nouvelle categorie</a>";

$ListeCategorie = $categories->afficheCategorie();
for ($i=0; $i < count($ListeCategorie) ; $i++) { 
	$idCategorie[$i]= $ListeCategorie[$i]->ID_CATEGORIE;
	$nomCategorie[$i] = $ListeCategorie[$i]->NOM_CATEGORIE;
	$descriptionCategorie[$i] = $ListeCategorie[$i]->DESCRIPTION;

	if (!empty($_SESSION['admin']))

{

		$edit[$i] = "
		<div class=\"aDroite btn\">
			<form class=\"memeLigne\" method=\"post\" action=\"?action=Formulaires/editerCategorie\" class=\"messages\">
			<input type=\"text\" name=\"idCategorie\" value =".$idCategorie[$i]."  style =\"display: none\"/> 
			<input class=\"btn bouton\" type=\"submit\" value=\"Editer\"/>
			</form>";

		$suppr[$i] = "
			<form class=\"memeLigne\" method=\"post\" action=\"?action=Formulaires/supprimerCategorie\" class=\"messages\">
			<input type=\"text\" name=\"idCategorie\" value =".$idCategorie[$i]."  style =\"display: none\"/> 
			<button class=\"btn bouton rouge\" type=\"submit\" name=\"submit\" value=\"valider\">
				<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"> Supprimer</span>
			</button>
			</form>
		</div>";
	}

	 	
	else {
		$suppr[$i]=" ";
		$edit[$i]=" ";
	}
}
?>