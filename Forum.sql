-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 19 Janvier 2015 à 12:43
-- Version du serveur: 5.5.38
-- Version de PHP: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `Forum`
--

-- --------------------------------------------------------

--
-- Structure de la table `CATEGORIE`
--

CREATE TABLE IF NOT EXISTS `CATEGORIE` (
  `ID_CATEGORIE` int(100) NOT NULL AUTO_INCREMENT,
  `NOM_CATEGORIE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_CATEGORIE`),
  UNIQUE KEY `NOM_CATEGORIE` (`NOM_CATEGORIE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `CATEGORIE`
--

INSERT INTO `CATEGORIE` (`ID_CATEGORIE`, `NOM_CATEGORIE`, `DESCRIPTION`) VALUES
(8, 'Categorie Jeux vidÃ©o', 'Ici nous parlerons de tous ce qui concerne les jeux vidÃ©o'),
(9, 'Cours', 'Pour parler de cours'),
(10, 'SantÃ©', 'Niveau mÃ©dical');

-- --------------------------------------------------------

--
-- Structure de la table `POST`
--

CREATE TABLE IF NOT EXISTS `POST` (
  `ID_POST` int(11) NOT NULL AUTO_INCREMENT,
  `ID_AUTEUR_POST` int(11) NOT NULL,
  `DATE_POST` datetime NOT NULL,
  `CONTENUE_POST` text COLLATE utf8_unicode_ci NOT NULL,
  `ID_CATEGORIE` int(100) NOT NULL,
  PRIMARY KEY (`ID_POST`),
  KEY `ID_AUTEUR_POST` (`ID_AUTEUR_POST`),
  KEY `POST` (`ID_CATEGORIE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Contenu de la table `POST`
--

INSERT INTO `POST` (`ID_POST`, `ID_AUTEUR_POST`, `DATE_POST`, `CONTENUE_POST`, `ID_CATEGORIE`) VALUES
(27, 1, '2015-01-14 12:21:02', 'salut boloss ;D		<br/>Edit:2015-01-17-05-11-46', 8),
(28, 1, '2015-01-17 11:23:29', 'Vive le php!', 9),
(29, 1, '2015-01-17 11:25:57', 'Mais trop :D\r\n', 9),
(30, 1, '2015-01-17 11:26:30', 'La santÃ© c''est ce qu''il y a de plus important !', 10),
(31, 5, '2015-01-17 11:26:55', 'Oui, l''important c''est de l''avoir.\r\n', 10),
(32, 5, '2015-01-17 11:29:26', 'Test du placeholder :D\r\n', 9);

-- --------------------------------------------------------

--
-- Structure de la table `USER`
--

CREATE TABLE IF NOT EXISTS `USER` (
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  `PSEUDO_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD_USER` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NOM_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PRENOM_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_USER`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `USER`
--

INSERT INTO `USER` (`ID_USER`, `PSEUDO_USER`, `PASSWORD_USER`, `NOM_USER`, `PRENOM_USER`, `MAIL`, `STATUS`) VALUES
(1, 'Abbalangel', '04c1d7cd203ef496f200ee5a096b5764', 'jeremie', 'jerem''', 'admin@admin.fr', 'admin'),
(5, 'Kanmuru', 'd8735f7489c94f42f508d7eb1c249584', 'gueri', 'gautier', 'gautier@gmail.com', '');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `POST`
--
ALTER TABLE `POST`
  ADD CONSTRAINT `POST` FOREIGN KEY (`ID_CATEGORIE`) REFERENCES `CATEGORIE` (`ID_CATEGORIE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `POST_ibfk_1` FOREIGN KEY (`ID_AUTEUR_POST`) REFERENCES `USER` (`ID_USER`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
