<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
		<title><?php echo $page_title ?> Forum</title>
		<link rel="stylesheet" href="./static/css/bootstrap.min.css" type="text/css" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="./static/css/monstyle.css" type="text/css" media="screen">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-2 well">
					<ul class="nav nav-pills nav-stacked">
						<li><a href="?action=home">Home</a></li>
						<li><?php echo $url; ?></li>
						<li><?php echo $link; ?></li>
					</ul>
				</div>
				<div class="col-md-10">
					<div class="page-header">
						<h1><?php echo $page_title; ?> </h1>
					</div>
					<?php echo $content ?>
				</div>
			</div>
		</div>
		<footer> Forum crée par J. Bouyer et G. Gueri</footer>
	</body>
</html>
