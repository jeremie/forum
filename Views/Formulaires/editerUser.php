<form method="post" action="?action=Formulaires/validerEditionUser" class="messages">
	
	<input type="text" name="iduser" value ="<?php echo $iduser?>"  style ="display: none"/> 
	<div class="input-group">
        	<label class="input-group-addon" for="pseudo">Pseudo :</label>
        	<input type="text" name="pseudo" value="<?php echo $pseudo; ?>" id="pseudo" size="30" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}" title="Veuillez entrer un pseudo de 20 caractères maximum et contenant des lettres, des chiffres ou - ou _ ou . !"/>
    </div>    
    <br/>
    <div class="input-group">
        	<label class="input-group-addon" for="Nom">Nom :</label>
        	<input type="text" name="nom" value="<?php echo $name; ?>" id="Nom" size="30"/>
    </div>
    <br/>
    <div class="input-group">
        	<label class="input-group-addon" for="prenom">Prenom :</label>
        	<input type="text" name="prenom" value="<?php echo $prenom; ?>" id="prenom" size="30"/>
    </div>
    <br/>	
	<div class="input-group">
			<label class="input-group-addon" for="mdp">Nouveau mot de passe :</label>
        	<input type="password" name="mdp" id="mdp" size="30" pattern=".[a-zA-Z0-9]{6,}" title="Veuillez entrer un mot de passe alphanumérique de 7 caractères minimum!" />
	</div>
    <br/>
    <div class="input-group">
    		<label class="input-group-addon" for="mail">Adresse email :</label>
			<input type="email" name="mail" value="<?php echo $mail; ?>" id="mail" size="30"/>
	</div>
	<br/>
	<div class="input-group">
    		<label class="input-group-addon" for="status">Status :</label>
			<input type="text" name="status" value="<?php echo $status; ?>" id="status" size="30"/>
	</div>
	<br/>
	<button class="btn bouton" type="submit" name="submit" value="valider">
		<span class="glyphicon glyphicon-ok" aria-hidden="true"> Valider</span>
	</button>	 
</form>
