<div class="alerteur">
		<?php echo $message; ?>
</div>
<form method="post" action="?action=Formulaires/creation" class="inscription">
    <div class="input-group">
        	<label class="input-group-addon" for="pseudo">Pseudo :</label>
        	<input type="text" name="pseudo" id="pseudo" size="30" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}" title="Veuillez entrer un pseudo de 20 caractères maximum et contenant des lettres, des chiffres ou - ou _ ou . !"/>
    </div>    
    <br/>
    <div class="input-group">
        	<label class="input-group-addon" for="Nom">Nom :</label>
        	<input type="text" name="Nom" id="Nom" size="30"/>
    </div>
    <br/>
    <div class="input-group">
        	<label class="input-group-addon" for="Prenom">Prenom :</label>
        	<input type="text" name="Prenom" id="Prenom" size="30"/>
    </div>
    <br/>	
	<div class="input-group">
			<label class="input-group-addon" for="mdp">Mot de passe :</label>
        	<input type="password" name="mdp" id="mdp" size="30" pattern=".[a-zA-Z0-9]{6,}" title="Veuillez entrer un mot de passe alphanumérique de 7 caractères minimum!" />
	</div>
	<br/>
	<div class="input-group">
			<label class="input-group-addon" for="mdp_confirm">Confirmez votre mot de passe.</label>
			<input type="password" name="mdp_confirm" id="mdp_confirm" size="30"/>
    </div>
    <br/>
    <div class="input-group">
    		<label class="input-group-addon" for="mail">Adresse email :</label>
			<input type="email" name="mail" id="mail" size="30"/>
	</div>
	<br/>
	<div class="input-group">
			<label class="input-group-addon" for="mail_confirm">Confirmez votre adresse email.</label>
        	<input type="email" name="mail_confirm" id="mail_confirm" size="30"/>
    </div>
    	<br/>
    	<br/>								
			<button class="btn bouton" type="submit" name="submit" value="valider">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"> Valider</span>
            </button>
    </div>
</form>
