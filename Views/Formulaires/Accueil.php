<!DOCTYPE html>
<html>
<head>
	<title>Connexion</title>
</head>
<body>
	<form action="?action=Formulaires/Accueil" method="post">
		<fieldset>
		<legend>Identifiez-vous</legend>
		<?php
		// Rencontre-t-on une erreur ?
		if(!empty($message))
		echo '<p>', htmlspecialchars($message) ,'</p>';
		?>
		<p>
			<div class="input-group">
				<label class="input-group-addon" for="login">Login :</label>
				<input type="text" name="login" id="login" value="" />
			</div>
		</p>
		<p>
			<div class="input-group">
				<label class="input-group-addon" for="password">Password :</label>
				<input type="password" name="password" id="password" value="" />
			</div>
			<br/>
			<button class="btn bouton" type="submit" name="submit" value="valider">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"> Valider</span>
			</button>
		</p>
		<p>Nouveau utilisateur? Cliquez 
			<a href="?action=Formulaires/creation">ici</a>
		</p>
		</fieldset>
	</form> 
</body>
</html>