<?php 
require_once 'auth.php';


class post extends auth{

private $message_ERROR;

	function get_message_ERROR(){
		return $this->message_ERROR;
	}

function afficheMessages($idcategorie){
	$i= 0;
		$db=$this->getbd();
	$requete="SELECT * FROM POST WHERE ID_CATEGORIE=?"; 
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return NULL;
   }
   $cate=$idcategorie;
   $stmt->bindparam(1,$cate);
   $stmt->execute();

	while ($result=$stmt->fetch(PDO::FETCH_OBJ)){
      $resultat[$i]= $result;
      $i+=1;
   }
   if (empty($resultat)){
   	return NULL;
   }
   return $resultat;
}




function saveMessage ($idCategorie,$idUser,$contenue_post){
	$db=$this->getbd();
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
    if(empty($contenue_post))
      {
        $message = 'Le message ne peut être vide';
      return false;
      }
    
    $req = $db->prepare('INSERT INTO POST (ID_AUTEUR_POST, DATE_POST, CONTENUE_POST, ID_CATEGORIE)
       VALUES( :idUser,:datepost, :contenue_post, :idCategorie)');
    
    $req->execute(array(
      'idUser' => $idUser,
      'datepost' => date('Y-m-d-h-i-s'),
      'contenue_post' => $contenue_post,
      'idCategorie' => $idCategorie
      
      ));
  return true;
}


function recupMessage($idPost){
	$db=$this->getbd();

 $requete="SELECT * FROM POST WHERE ID_POST=?"; 
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }
   $id=$idPost;
   $stmt->bindparam(1,$id);
   $stmt->execute();

   if(($result=$stmt->fetch(PDO::FETCH_OBJ)))
    return $result;
}

function recupAllMessage(){
  $db=$this->getbd();
  $i=0;

 $requete="SELECT * FROM POST"; 
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }
   $stmt->execute();

  while ($result=$stmt->fetch(PDO::FETCH_OBJ)){
      $resultat[$i]= $result;
      $i+=1;
   }
   if (empty($resultat)){
    return NULL;
   }
   return $resultat;
}


function editPost($idPost,$contenuePost){

	$db=$this->getbd();
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
	$requete = "UPDATE POST SET  CONTENUE_POST= ? WHERE ID_POST = ?";
  $id = $idPost;
  $message = $contenuePost."<br/>Edit:".date('Y-m-d-h-i-s');

 if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table post";
       return false;
     }

  $stmt->bindparam(1,$message);
  $stmt->bindparam(2,$id);
  $stmt->execute();
 

  if(!$result=$stmt->fetch(PDO::FETCH_OBJ))
    return true;

  return false;
}


function deletePost($idPost){


	$db=$this->getbd();
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
   
   $requete="DELETE FROM POST WHERE ID_POST=?";	
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }
   $id=$idPost;

   $stmt->bindparam(1,$id);
   $stmt->execute();
   if(($result=$stmt->fetch(PDO::FETCH_OBJ))){
     $message = 'id incorrect';
     return true;
   }
   // stockage dans la session des infos de la base de données
    return false;
  }

} 

?>