<?php 
require_once 'auth.php';

class categorie extends auth{

private $message_ERROR;

	function get_message_ERROR(){
		return $this->message_ERROR;
	}

function afficheCategorie(){
	$i= 0;

	 $db=$this->getbd();
	$requete="SELECT * FROM CATEGORIE"; 
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }

   $stmt->execute();

	while ($result=$stmt->fetch(PDO::FETCH_OBJ)){
      $resultat[$i]= $result;
      $i+=1;
   }
   if (!empty($resultat))
    return $resultat;
  else
    return null;
}


function recupCategorie($idCategorie){
  $db=$this->getbd();
  $requete="SELECT * FROM CATEGORIE WHERE ID_CATEGORIE=?"; 
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }
   $id=$idCategorie;
   $stmt->bindparam(1,$id);
   $stmt->execute();

   if(($result=$stmt->fetch(PDO::FETCH_OBJ)))
    return $result;
}


function editCategorie($nomCategorie,$description,$idCate){
  $db=$this->getbd();	
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
	$requete = "UPDATE CATEGORIE SET  DESCRIPTION= ?, NOM_CATEGORIE=? WHERE ID_CATEGORIE = ?";
  $id = $idCate;
  $nom=$nomCategorie;
  $descr = $description;

 if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table post";
       return false;
     }

  $stmt->bindparam(1,$descr);
  $stmt->bindparam(2,$nom);
  $stmt->bindparam(3,$id);
  $stmt->execute();
 
  if(!$result=$stmt->fetch(PDO::FETCH_OBJ))
    return true;

  return false;
}


function deleteCategorie($idCategorie){

  $db=$this->getbd();	
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
   
   $requete="DELETE FROM CATEGORIE WHERE ID_CATEGORIE=?";	
   if(!($stmt=$db->prepare($requete))){
     $message = "erreur d'accès à la table users";
     return false;
   }
   $id=$idCategorie;

   $stmt->bindparam(1,$id);
   $stmt->execute();
   
   if(($result=$stmt->fetch(PDO::FETCH_OBJ))){
     $message = 'id incorrect';
     return true;
   }
   // stockage dans la session des infos de la base de données
    return false;
  }


function ajoutCategorie($nom, $description){

  $db=$this->getbd(); 
    // On selectionne dans la base le champs correspondant au nom d'utilisateur.
    if(empty($nom) || empty($description))
      {
        $this->message = 'remplir tous les champs.';
      return false;
      }

      
   $requete="SELECT * FROM CATEGORIE WHERE NOM_CATEGORIE=?"; 
   if(!($stmt=$db->prepare($requete))){
     $this->message = "erreur d'accès à la table users";
     return false;
   }
   $nomCate=$nom;
   $stmt->bindparam(1,$nomCate);
   $stmt->execute();
   
   if(!($result=$stmt->fetch(PDO::FETCH_OBJ))){
    $this->message = "La categorie n'existe pas";
    
    $req = $db->prepare('INSERT INTO CATEGORIE (NOM_CATEGORIE, DESCRIPTION)
       VALUES(:nom, :description)');
    
    $req->execute(array(
      'nom' => $nom,
      'description' => $description
      ));
    }
    else {

      $this->message = "Une catégorie posséde deja ce nom";

    return false;
    }
  return true;
  }
}

?>