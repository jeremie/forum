<?php 
require_once 'auth.php';


class user extends auth{
private $message_ERROR;

	function get_message_ERROR(){
		return $this->message_ERROR;
	}


  function AuthUser($user_input, $pass_input){
   $db=$this->getbd();
      // On selectionne dans la base le champs correspondant au nom d'utilisateur.
      if(empty($user_input) || empty($pass_input))
        {
  	$this->message_ERROR = 'remplir tous les champs.';
  	return false;
        }
     
     $requete="SELECT * FROM USER WHERE PSEUDO_USER=?";	
     if(!($stmt=$db->prepare($requete))){
     	$this->message_ERROR = "erreur d'accès à la table users";
      return false;
     }
     $user=$user_input;
     $stmt->bindparam(1,$user);
     $stmt->execute();
     if(! ($result=$stmt->fetch(PDO::FETCH_OBJ))){
       $this->message_ERROR = 'login incorrect';
       return false;
     }
     if(md5($pass_input)!=$result->PASSWORD_USER){
       $this->message_ERROR ="Mot de passe incorect";
        return false;
        }
     // stockage dans la session des infos de la base de données
      $_SESSION['login']=$result->NOM_USER;
      $_SESSION['admin']=$result->STATUS;
      $_SESSION['id']=$result->ID_USER;
      $_SESSION['session_id']=session_id();
        
      return true;
   }





  function AddUser($pseudo, $passwd, $passwd2, $mail, $mail2, $nom, $prenom){

   $db=$this->getbd();
      // On selectionne dans la base le champs correspondant au nom d'utilisateur.
      if(empty($pseudo) || empty($passwd) || empty($passwd2) || empty($mail) || empty($mail2) || empty($nom) || empty($prenom) )
        {
          $this->message = 'Veuillez remplir tous les champs.';
        return false;
        }

      if($passwd != $passwd2)
       {
        $this->message = "Les deux mots de passe doivent être identique";
        return false;
        }

     if($mail!= $mail2)
        {
          $this->message = "Les e-mails doivent être identique";
        return false;
        }
        
        
     $requete="SELECT * FROM USER WHERE PSEUDO_USER=?"; 
     if(!($stmt=$db->prepare($requete))){
       $this->message = "erreur d'accès à la table users";
       return false;
     }
     $user=$pseudo;
     $stmt->bindparam(1,$user);
     $stmt->execute();
     
     if(!($result=$stmt->fetch(PDO::FETCH_OBJ))){
      $this->message = "Le login n'existe pas";
       //verification de l'adresse mail
      
      $req = $db->prepare('INSERT INTO USER (PSEUDO_USER, PASSWORD_USER, NOM_USER, PRENOM_USER, MAIL)
         VALUES(:pseudo, :mdp, :nom, :prenom, :mail)');
      
      $req->execute(array(
        'pseudo' => $pseudo,
        'mdp' => md5($passwd),
        'mail' => $mail,
        'nom' => $nom,
        'prenom' => $prenom,
        ));
      }
      else {

        $this->message = "Un utilisateur posséde déjà ce pseudo";
      
      return false;
      }

    return true;
    }

    
  function afficheUser($idUser)
  {
   $db=$this->getbd();
    $requete="SELECT * FROM USER WHERE ID_USER=?"; 
     if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table users";
       return NULL;
     }
     $idd=$idUser;
     $stmt->bindparam(1,$idd);
     $stmt->execute();

    if ( !$result=$stmt->fetch(PDO::FETCH_OBJ)){
        return NULL;
     }

     return $result;
  }


  function afficheNomUser($idUser)
  {
   $db=$this->getbd();
    $requete="SELECT PSEUDO_USER FROM USER WHERE ID_USER=?"; 
     if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table users";
       return NULL;
     }
     $idd=$idUser;
     $stmt->bindparam(1,$idd);
     $stmt->execute();

    if ( !$result=$stmt->fetch(PDO::FETCH_OBJ)){
        return NULL;
     }

     return $result;
  }



  function afficheAllUsers()
  {
    $db=$this->getbd();
    $requete="SELECT * FROM USER"; 
    if(!($stmt=$db->prepare($requete))){
      $message = "erreur d'accès à la table users";
      return NULL;
    }
    $stmt->execute();
    if ( !$result=$stmt->fetchAll(PDO::FETCH_OBJ)){
        return NULL;
     }

    return $result;
  }

  function recupUser($idUser)
  {
    $db=$this->getbd();
    $requete="SELECT * FROM USER WHERE ID_USER=?"; 
    if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table users";
       return false;
    }
    $id=$idUser;
    $stmt->bindparam(1,$id);
    $stmt->execute();

    if(($result=$stmt->fetch(PDO::FETCH_OBJ)))
      return $result; 
  }

  function editerUser($idUser,$pseudo,$passwd,$name,$prenom,$mail,$status)
  {
    $db=$this->getbd();
    $req=$db->prepare("UPDATE USER SET ID_USER=:id,PSEUDO_USER=:pseudo,PASSWORD_USER=:mdp,NOM_USER=:nom,PRENOM_USER=:prenom,MAIL=:mail,STATUS=:status WHERE ID_USER=:id");
      
    $req->execute(array(
        'id' => $idUser,
        'pseudo' => $pseudo,
        'mdp' => md5($passwd),
        'mail' => $mail,
        'nom' => $name,
        'prenom' => $prenom,
        'status' => $status,
      ));
    return true;
  }

  function deleteUser($idUser)
  {
    $db=$this->getbd();
    $requete="DELETE FROM USER WHERE ID_USER=?"; 
    $idd=$idUser;
    if(!($stmt=$db->prepare($requete))){
       $message = "erreur d'accès à la table users";
       return NULL;
     }
    $stmt->bindparam(1,$idd);
    $stmt->execute();
  }

}


?>
