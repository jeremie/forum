-- phpMyAdmin SQL Dump
-- version 4.3.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 16 Décembre 2014 à 17:52
-- Version du serveur :  5.5.40-0ubuntu0.14.04.1
-- Version de PHP :  5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `Forum`
--

-- --------------------------------------------------------

--
-- Structure de la table `CATEGORIE`
--

CREATE TABLE IF NOT EXISTS `CATEGORIE` (
  `NOM_CATEGORIE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `POST`
--

CREATE TABLE IF NOT EXISTS `POST` (
  `ID_POST` int(11) NOT NULL,
  `ID_AUTEUR_POST` int(11) NOT NULL,
  `DATE_POST` date NOT NULL,
  `CONTENUE_POST` text COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORIE_POST` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `USER`
--

CREATE TABLE IF NOT EXISTS `USER` (
  `ID_USER` int(11) NOT NULL,
  `PSEUDO_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD_USER` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `NOM_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PRENOM_USER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CATEGORIE`
--
ALTER TABLE `CATEGORIE`
 ADD PRIMARY KEY (`NOM_CATEGORIE`);

--
-- Index pour la table `POST`
--
ALTER TABLE `POST`
 ADD PRIMARY KEY (`ID_POST`), ADD KEY `ID_AUTEUR_POST` (`ID_AUTEUR_POST`);

--
-- Index pour la table `USER`
--
ALTER TABLE `USER`
 ADD PRIMARY KEY (`ID_USER`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `POST`
--
ALTER TABLE `POST`
ADD CONSTRAINT `POST_ibfk_1` FOREIGN KEY (`ID_AUTEUR_POST`) REFERENCES `USER` (`ID_USER`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;